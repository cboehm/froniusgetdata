# froniusGetData
This project provides a ETL process to collect data from Fronius API from a photovoltaics system and store it to a Influx timeseries database.
Therefore an Airflow Image is build to provide ARM based devices like a Raspberry Pi cause there is no official Image with ARM support.

## Howto
### prepare Raspberry Pi

Install all updates
~~~
sudo apt update && sudo apt upgrade
~~~

Install git
~~~
sudo apt install git
~~~

Install docker
~~~
curl -fsSL https://get.docker.com | sh
~~~

Add user pi to docker group
~~~
sudo groupadd docker
sudo gpasswd -a pi docker
~~~

logon and logoff or do a full reboot
~~~
reboot
~~~

### install froniusGetData

Clone this git repository and move to this directory
~~~
git clone https://gitlab.com/cboehm/froniusgetdata.git
cd ./froniusgetdata
~~~

Copy pi.template.env, name it pi.env and replace the values inside <> with your local configuration.
~~~
cp pi.template.env pi.env
~~~

Initial Docker dependencies
~~~
make init_network
~~~

Run container with Influx Database
~~~
make run_influx
~~~

Run container with Chronograf Dashboard
~~~
make run_chronograf
~~~

Setup Influx DB interactively via requesting <IP>:8888
You have to add the user and password of the database "Configuration".
Next add the database "fronius" at section "InfluxDB Admin".

### (Optional) Build the image locally for ARM devices
#### Activate buildx for Docker
Source: https://medium.com/@artur.klauser/building-multi-architecture-docker-images-with-buildx-27d80f7e2408

This is (atm) an experimental feature of Docker. To activate it add this to your config file at $HOME/.docker/config.json
~~~
{
  …
  "experimental" : "enabled"
}
~~~

Run a docker image with QEMU installed.
~~~
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
~~~

Register a new builder instance for qemu
~~~
docker buildx create --name myQemuBuilder
docker buildx use mybuilder
~~~

Check whether it's fully installed
~~~
docker buildx inspect --bootstrap
docker buildx ls
~~~

## Gitlab CI / CD Pipeline for Cross Compiling ARM images
### Building the buildx docker image for building locally or via Gitlab Pipeline
Build the image in ./buildImage/ and push it to your container registry of your Gitlab project.
~~~
make build_push_buildimage
~~~

### use a local Gitlab Runner
The build of the Airflow base image for ARM platform may need more than the provided buildtime provided by the shared Gitlab Runner. (3 hours maximum)
Then the only way for building is using a own Gitlab Runner cause you can't increase the max build time for shared runners.
 
### Install / Register local Gitlab Runner
pre-requisites:
- installed Docker environment

source:
https://docs.gitlab.com/runner/install/docker.html
https://docs.gitlab.com/runner/register/index.html#docker
~~~
docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
	 
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
~~~

### Further configuration
**tagged Runner**
If you tagged your runner, you have either to enhance you Gitlab Pipeline
https://docs.gitlab.com/ee/ci/yaml/#tags
~~~
  tags:
    - yourTaggedRunner
~~~

or you check the box "Run untagged jobs" at the section:
https://gitlab.com/<gitlabUser>/<project>/runners/<runnerId>/edit

### Error connecting docker host
source: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6295#note_453135562

edit config.toml and change volumes option
before
~~~
[[runners]]
  ...
  [runners.docker]
    volumes = ["/cache"]
    ...
~~~

after
~~~
[[runners]]
  ...
  [runners.docker]
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    ...
~~~
macOS may be configured differently, take a look at this Use Docker socket binding

## Some details

### Airflow config
Airflow can be configurated via a config file or via setting Environment Variables inside the dockerfile or entrypoint.sh
https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html

### Airflow Maintenance
Airflow to delete logs or refresh some of the airflow processes. 
https://github.com/teamclairvoyant/airflow-maintenance-dags

You have to change the entries at the ALERT_EMAIL_ADDRESSES array of you want to get information about alerts.

The webserver is a bit buggy. The processes will run even without it. To restart it just do
~~~
docker exec froniusAirflow airflow webserver
~~~

### Chronograph
If you create a Dashboard with a Single stat it maybe always 0 and this helps eventually.
https://github.com/influxdata/chronograf/issues/1493#issuecomment-303835817

Or you get the last value directly
SELECT "PAC" FROM "fronius"."autogen"."froniusUmrichter" GROUP BY time(:interval:) ORDER BY DESC LIMIT 1


# Howto Upgrade Influx 1.x to Influx 2.x
pi.env anpassen

Vorgehen
1. Upgrade Grafana & test functionality
2. Upgrade Influx & test functionality in Chronograph dashboard
3. Generarte dedicated auth keys for Grafana and Airflow jobs 
4. Configure auth keys in env file and Grafana config
5. Restart Airflow container