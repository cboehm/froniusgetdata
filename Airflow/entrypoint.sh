#!/bin/bash
export AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION=False
export FRONIUS_BASE_URL=$froniusBaseUrl
export INFLUX_URL=$influxUrl
export INFLUX_TOKEN=$influxToken

airflow initdb
airflow scheduler &
exec airflow webserver