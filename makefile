froniusBaseUrl = ""
influxUrl = ""
influxLocation = "froniusDb"
influxToken = ""
influxDatabase = ""
influxAdmin = ""
influxAdminPassword = ""
influxUser = ""
influxUserPassword = ""
chronographLocation = "froniusDashboard"
grafanaLocation = "froniusGrafana"
grafanaAdmin = ""
grafanaAdminPassword = ""
containerRegistryUrl = ""

# load environment file overriding default values
include pi.env

init_network:
	docker network create -d bridge froniusNet

# local_etl:
# 	pip install influxdb-client

# run_influx_old:
# 	docker run -p 8086:8086 \
#       -v froniusDb:/var/lib/influxdb \
# 	  --network froniusNet \
# 	  -d \
# 	  --name froniusDb \
#       quay.io/influxdb/influxdb:v2.0.2 \
# 	  --reporting-disabled

run_influx:
	docker run -d \
		--restart always \
		-p 8086:8086 \
		--network froniusNet \
		--env INFLUXDB_DB=$(influxDatabase) \
		--env INFLUXDB_ADMIN_USER=$(influxAdmin) \
		--env INFLUXDB_ADMIN_PASSWORD=$(influxAdminPassword) \
		--env INFLUXDB_USER=$(influxUser) \
		--env INFLUXDB_USER_PASSWORD=$(influxUserPassword) \
		-v $(influxLocation):/var/lib/influxdb \
		--name froniusDb \
		influxdb:1.8 -config /etc/influxdb/influxdb.conf
#   -v $$PWD/influxDb/influxdb.conf:/etc/influxdb/influxdb.conf:ro \

run_influx2_upgrade:
	docker run \
		-p 8086:8086 \
		--network froniusNet \
		--restart always \
		-v $(influxLocation):/var/lib/influxdb \
		-v $(influx2Location):/var/lib/influxdb2 \
		-v $(influxConfigLocation):/etc/influxdb/influxdb.conf \
		-e DOCKER_INFLUXDB_INIT_MODE=upgrade \
		-e DOCKER_INFLUXDB_INIT_USERNAME=$(influxAdmin) \
		-e DOCKER_INFLUXDB_INIT_PASSWORD=$(influxAdminPassword) \
		-e DOCKER_INFLUXDB_INIT_ORG=$(influxInitOrg) \
		-e DOCKER_INFLUXDB_INIT_BUCKET=$(influxInitBucket) \
		--name froniusDb \
		influxdb:2.1

run_influx2:
	docker run -d \
		-p 8086:8086 \
		--network froniusNet \
		--restart always \
		-v $(influx2Location):/var/lib/influxdb2 \
		-v $(influxConfigLocation):/etc/influxdb/influxdb.conf \
		-e DOCKER_INFLUXDB_INIT_USERNAME=$(influxAdmin) \
		-e DOCKER_INFLUXDB_INIT_PASSWORD=$(influxAdminPassword) \
		-e DOCKER_INFLUXDB_INIT_ORG=$(influxInitOrg) \
		-e DOCKER_INFLUXDB_INIT_BUCKET=$(influxInitBucket) \
		--name froniusDb \
		influxdb:2.1


run_chronograf:
	docker run -d \
		--restart always \
		-p 8888:8888 \
		--network froniusNet \
		-v $(chronographLocation):/var/lib/chronograf \
		--name froniusDashboard \
		chronograf --influxdb-url=$(influxUrl)

run_grafana:
	docker run -d \
		--restart always \
		-p 3000:3000 \
		--network froniusNet \
        --user 104 \
		-v $(grafanaLocation):/var/lib/grafana \
		--env GF_SECURITY_ADMIN_USER=$(grafanaAdmin) \
		--env GF_SECURITY_ADMIN_PASSWORD=$(grafanaAdminPassword) \
		--name froniusGrafana \
		grafana/grafana:8.4.3

# grafana/grafana:7.5.15

build_push_buildimage:
	- docker build ./buildImage/ \
		--tag $(containerRegistryUrl)/docker:buildx
	- docker login registry.gitlab.com
	- docker push $(containerRegistryUrl)/docker:buildx
	
build_airflow:
	docker build --no-cache \
	--build-arg PYTHON_MAJOR_MINOR_VERSION=3.8 \
	--build-arg BUILD_MACHINE=raspberry \
	./Airflow/ \
	 --tag froniusairflow

# --no-cache
buildx_airflow:
	docker buildx build \
	--build-arg PYTHON_MAJOR_MINOR_VERSION=3.8 \
	--build-arg BUILD_MACHINE=raspberry \
	--platform linux/arm/v7 \
	--builder myQemuBuilder \
	--tag froniusairflow \
	./Airflow/ \
	--load
	# --push

init_buildx:
	# - docker buildx create --name mybuilder
	# - docker buildx use mybuilder
	# - docker buildx inspect --bootstrap
	- docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
	- docker buildx rm myArmBuilder
	- docker buildx create --name myArmBuilder --driver docker-container --use
	- docker buildx inspect --bootstrap

run_airflow:
	docker run -it -d \
	-v $$PWD/etl:/app/airflow/dags \
	-e froniusBaseUrl=$(froniusBaseUrl) \
	-e influxUrl=$(influxUrl) \
	-e influxToken=$(influxToken) \
	-p 8080:8080 \
	--network froniusNet \
	--name froniusAirflow \
	froniusairflow

run_airflowx:
	- docker login registry.gitlab.com -u $(gitlabDockerContainerUsername) -p $(gitlabDockerContainerToken)
	- docker run -it -d \
		-v $$PWD/etl:/app/airflow/dags \
		-e froniusBaseUrl=$(froniusBaseUrl) \
		-e influxUrl=$(influxUrl) \
		-e influxToken=$(influxToken) \
		-p 8080:8080 \
		--network froniusNet \
		--name froniusAirflow \
		registry.gitlab.com/cboehm/froniusgetdata/froniusgetdata:latest

clean_airflow:
	docker container stop froniusAirflow
	docker container rm froniusAirflow

clean_chronograph:
	docker container stop froniusDashboard
	docker container rm froniusDashboard

clean_influx:
	docker container stop froniusDb
	docker container rm froniusDb

clean_grafana:
	docker container stop froniusGrafana
	docker container rm froniusGrafana
# docker volume rm $(grafanaLocation)
