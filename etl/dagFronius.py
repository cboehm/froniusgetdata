from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from etlFronius import transferOnePoint
from datetime import datetime
import os


with DAG('python_dag', description='Fronius DAG', schedule_interval='*/1 * * * *', start_date=datetime(2018, 11, 1), catchup=False) as dag:
	#dummy_task 	= DummyOperator(task_id='dummy_task', retries=3)
	getInverterRealtimeData_task = PythonOperator(task_id='getInverterRealtimeData_task', python_callable=transferOnePoint.getInverterRealtimeData)
	getPowerFlowRealtimeData_task = PythonOperator(task_id='getPowerFlowRealtimeData_task', python_callable=transferOnePoint.getPowerFlowRealtimeData)
	getMeterRealtimeData_task = PythonOperator(task_id='getMeterRealtimeData_task', python_callable=transferOnePoint.getMeterRealtimeData)

	#getInverterRealtimeData_task >> getPowerFlowRealtimeData_task
	getInverterRealtimeData_task
	getPowerFlowRealtimeData_task
	getMeterRealtimeData_task