import requests
import os

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# environments variables
froniusBaseUrl = os.getenv('FRONIUS_BASE_URL', "http://127.0.0.1/solar_api/v1")
influxUrl = os.getenv('INFLUX_URL', "http://froniusDb:8086")
influxToken = os.getenv('INFLUX_TOKEN', "")

# global definitions
org = "home" #"private"
bucket = "fronius"

payload = {}
headers= {}

def getInverterRealtimeData():

    #baseUrl = "http://192.168.220.25/solar_api/v1"

    paramsGetInverterRealtimeData = {
        'Scope': 'Device',
        'DeviceId': 1,
        'DataCollection': 'CommonInverterData'
    }

    ## GetInverterRealtimeData
    response = requests.request("GET", froniusBaseUrl + '/GetInverterRealtimeData.cgi', headers=headers, data = payload, params = paramsGetInverterRealtimeData)


    #print(response.text.encode('utf8'))

    currentPower = response.json()['Body']['Data']['PAC']['Value']
    print(currentPower)

    currentDateTime = response.json()['Head']['Timestamp']
    print(currentDateTime)

    

    #print(response.json())

    # influxDb
    # user: froniusEtl
    # pw: froniusEtl
    # bucket: fronius
    # organization: private

    # from datetime import datetime

    # http://localhost:8086
    client = InfluxDBClient(url=influxUrl, token=influxToken)
    write_api = client.write_api(write_options=SYNCHRONOUS)

    # point = Point("froniusUmrichter") \
    # .tag("host", "inverterRealtimeData") \
    # .field("currentPower", currentPower) \
    # .time(currentDateTime, WritePrecision.NS)

    # point = Point("froniusUmrichter") \
    #     .tag("host", "inverterRealtimeData") \
    #     .field("InverterState", response.json()['Body']['Data']['DeviceStatus']['InverterState']) \
    #     .field("FAC", response.json()['Body']['Data']['FAC']['Value']) \
    #     .field("IAC", response.json()['Body']['Data']['IAC']['Value']) \
    #     .field("IAC_L1", response.json()['Body']['Data']['IAC_L1']['Value']) \
    #     .field("IAC_L2", response.json()['Body']['Data']['IAC_L2']['Value']) \
    #     .field("IAC_L3", response.json()['Body']['Data']['IAC_L3']['Value']) \
    #     .field("IDC", response.json()['Body']['Data']['IDC']['Value']) \
    #     .field("IDC_2", response.json()['Body']['Data']['IDC_2']['Value']) \
    #     .field("PAC", response.json()['Body']['Data']['PAC']['Value']) \
    #     .field("SAC", response.json()['Body']['Data']['SAC']['Value']) \
    #     .field("UAC", response.json()['Body']['Data']['UAC']['Value']) \
    #     .field("UAC_L1", response.json()['Body']['Data']['UAC_L1']['Value']) \
    #     .field("UAC_L2", response.json()['Body']['Data']['UAC_L2']['Value']) \
    #     .field("UDC", response.json()['Body']['Data']['UDC']['Value']) \
    #     .field("UDC_2", response.json()['Body']['Data']['UDC_2']['Value']) \
    #     .time(currentDateTime, WritePrecision.NS)


    fieldList = ["FAC", "IAC", "IAC_L1", "IAC_L2", "IAC_L3", "IDC", "IDC_2", "PAC", "SAC", "UAC", "UAC_L1", "UAC_L2", "UDC", "UDC_2"]

    point = Point("froniusUmrichter") \
        .tag("host", "inverterRealtimeData") \
        .field("InverterState", response.json()['Body']['Data']['DeviceStatus']['InverterState']) \
        .time(currentDateTime, WritePrecision.NS)

    for field in fieldList:
        if field in response.json()['Body']['Data']:
            point.field(field, response.json()['Body']['Data'][field]['Value'])


    write_api.write(bucket, org, point)

def getPowerFlowRealtimeData():

    paramsGetPowerFlowRealtimeData = {
    }

    ## GetInverterRealtimeData
    response = requests.request("GET", froniusBaseUrl + '/GetPowerFlowRealtimeData.fcgi', headers=headers, data = payload, params = paramsGetPowerFlowRealtimeData)

    #print(response.json())

    currentDateTime = response.json()['Head']['Timestamp']
    print(currentDateTime)

    client = InfluxDBClient(url=influxUrl, token=influxToken)
    write_api = client.write_api(write_options=SYNCHRONOUS)

    point = Point("froniusUmrichter") \
        .tag("host", "powerFlowRealtimeData") \
        .field("Battery_Mode", response.json()['Body']['Data']['Inverters']['1']['Battery_Mode']) \
        .field("DT", response.json()['Body']['Data']['Inverters']['1']['DT']) \
        .field("P", int(response.json()['Body']['Data']['Inverters']['1']['P'])) \
        .field("SOC", response.json()['Body']['Data']['Inverters']['1']['SOC']) \
        .field("BackupMode", response.json()['Body']['Data']['Site']['BackupMode']) \
        .field("BatteryStandby", response.json()['Body']['Data']['Site']['BatteryStandby']) \
        .field("Meter_Location", response.json()['Body']['Data']['Site']['Meter_Location']) \
        .field("Mode", response.json()['Body']['Data']['Site']['Mode']) \
        .field("P_Akku", response.json()['Body']['Data']['Site']['P_Akku']) \
        .field("P_Grid", response.json()['Body']['Data']['Site']['P_Grid']) \
        .field("P_Load", response.json()['Body']['Data']['Site']['P_Load']) \
        .field("P_PV", response.json()['Body']['Data']['Site']['P_PV']) \
        .field("rel_Autonomy", response.json()['Body']['Data']['Site']['rel_Autonomy']) \
        .field("rel_SelfConsumption", response.json()['Body']['Data']['Site']['rel_SelfConsumption']) \
        .time(currentDateTime, WritePrecision.NS)

    write_api.write(bucket, org, point)

def getMeterRealtimeData():

    paramsGetMeterRealtimeData = {
        'Scope': 'Device',
        'DeviceId': 0
    }

    ## GetInverterRealtimeData
    response = requests.request("GET", froniusBaseUrl + '/GetMeterRealtimeData.cgi', headers=headers, data = payload, params = paramsGetMeterRealtimeData)

    print(response.text)

    currentDateTime = response.json()['Head']['Timestamp']
    print(currentDateTime)

    client = InfluxDBClient(url=influxUrl, token=influxToken)
    write_api = client.write_api(write_options=SYNCHRONOUS)

    point = Point("froniusUmrichter") \
        .tag("host", "meterRealtimeData") \
        .field("Current_AC_Phase_1", response.json()['Body']['Data']['Current_AC_Phase_1']) \
        .field("Current_AC_Phase_2", response.json()['Body']['Data']['Current_AC_Phase_2']) \
        .field("Current_AC_Phase_3", response.json()['Body']['Data']['Current_AC_Phase_3']) \
        .field("EnergyReactive_VArAC_Sum_Consumed", response.json()['Body']['Data']['EnergyReactive_VArAC_Sum_Consumed']) \
        .field("EnergyReactive_VArAC_Sum_Produced", response.json()['Body']['Data']['EnergyReactive_VArAC_Sum_Produced']) \
        .field("EnergyReal_WAC_Minus_Absolute", response.json()['Body']['Data']['EnergyReal_WAC_Minus_Absolute']) \
        .field("EnergyReal_WAC_Plus_Absolute", response.json()['Body']['Data']['EnergyReal_WAC_Plus_Absolute']) \
        .field("EnergyReal_WAC_Sum_Consumed", response.json()['Body']['Data']['EnergyReal_WAC_Sum_Consumed']) \
        .field("EnergyReal_WAC_Sum_Produced", response.json()['Body']['Data']['EnergyReal_WAC_Sum_Produced']) \
        .field("Frequency_Phase_Average", response.json()['Body']['Data']['Frequency_Phase_Average']) \
        .field("Meter_Location_Current", response.json()['Body']['Data']['Meter_Location_Current']) \
        .field("PowerApparent_S_Phase_1", response.json()['Body']['Data']['PowerApparent_S_Phase_1']) \
        .field("PowerApparent_S_Phase_2", response.json()['Body']['Data']['PowerApparent_S_Phase_2']) \
        .field("PowerApparent_S_Phase_3", response.json()['Body']['Data']['PowerApparent_S_Phase_3']) \
        .field("PowerApparent_S_Sum", response.json()['Body']['Data']['PowerApparent_S_Sum']) \
        .field("PowerFactor_Phase_1", response.json()['Body']['Data']['PowerFactor_Phase_1']) \
        .field("PowerFactor_Phase_2", response.json()['Body']['Data']['PowerFactor_Phase_2']) \
        .field("PowerFactor_Phase_3", response.json()['Body']['Data']['PowerFactor_Phase_3']) \
        .field("PowerFactor_Sum", response.json()['Body']['Data']['PowerFactor_Sum']) \
        .field("PowerReactive_Q_Phase_1", response.json()['Body']['Data']['PowerReactive_Q_Phase_1']) \
        .field("PowerReactive_Q_Phase_2", response.json()['Body']['Data']['PowerReactive_Q_Phase_2']) \
        .field("PowerReactive_Q_Phase_3", response.json()['Body']['Data']['PowerReactive_Q_Phase_3']) \
        .field("PowerReactive_Q_Sum", response.json()['Body']['Data']['PowerReactive_Q_Sum']) \
        .field("PowerReal_P_Phase_1", response.json()['Body']['Data']['PowerReal_P_Phase_1']) \
        .field("PowerReal_P_Phase_2", response.json()['Body']['Data']['PowerReal_P_Phase_2']) \
        .field("PowerReal_P_Phase_3", response.json()['Body']['Data']['PowerReal_P_Phase_3']) \
        .field("PowerReal_P_Sum", response.json()['Body']['Data']['PowerReal_P_Sum']) \
        .time(currentDateTime, WritePrecision.NS)

    write_api.write(bucket, org, point)